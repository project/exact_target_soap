<?php
/**
 * @file
 * Provides abstraction around the ExactTarget SOAP API.
 */
class ExactTarget {

  // ExactTarget WSDL.
  public $wsdl = 'https://webservice.s4.exacttarget.com/etframework.wsdl';

  // The SoapClient used to make SOAP calls.
  public $client;

  // Set to TRUE to generate stack traces for SoapFaults.
  public $trace = TRUE;

  /**
   * ExactTarget Singleton for SoapClient.
   */
  public static function instance() {
    static $instance = NULL;

    if (is_null($instance)) {
      $instance = new ExactTarget();
    }

    return $instance;
  }

  /**
   *  Creates a new ExactTarget instance.
   */
  public function __construct() {
    $this->wsdl = variable_get('exact_target_soap_wsdl', $this->wsdl);
    try {
      // Ideally ExactTarget will support compression at some point, so this array will
      // need the following added: 'compression' => SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP | 5.
      $options = array('trace' => $this->trace);
      // In case of proxy server being used
      $proxy_host = variable_get('exact_target_soap_proxy_host', '');
      if (!empty($proxy_host)) {
        $options['proxy_host'] = $proxy_host;
        $port = variable_get('exact_target_soap_proxy_port', '');
        if (!empty($port)) {
          $options['proxy_port'] = $port;
        }
        $proxy_login = variable_get('exact_target_soap_proxy_login', '');
        if (!empty($proxy_login)) {
          $options['proxy_login'] = $proxy_login;
        }
        $proxy_pass = variable_get('exact_target_soap_proxy_password', '');
        if (!empty($proxy_pass)) {
          $options['proxy_password'] = $proxy_pass;
        }
      }
      $this->client = new ExactTargetSoapClient($this->wsdl, $options);
      $this->client->username = variable_get('exact_target_soap_username', '');
      $this->client->password = variable_get('exact_target_soap_password', '');

    } catch (Exception $e) {
      // Make sure the API is responding.
      exact_target_soap_status();
    }
  }

  /**
   * Calls an ExactTarget SOAP method.
   */
  public function __call($method, $params) {
    $bypass_batch = FALSE;
    if (isset($params[0]->exact_target_soap_bypass_batch)) {
      $bypass_batch = $params[0]->exact_target_soap_bypass_batch;
      unset($params[0]->exact_target_soap_bypass_batch);
    }
    $uids = array();
    foreach ($params as $et_request_obj) {
      if (isset($et_request_obj->uids)) {
        $uids = array_merge($et_request_obj->uids, $uids);
        unset($et_request_obj->uids);
      }
    }
    $batch = variable_get('exact_target_soap_batch', FALSE);
    if (!$bypass_batch && $method != 'GetSystemStatus' && !empty($batch)) {
      watchdog('exact_target_soap', 'Method ' . $method . ' is being queued');
      // We are queuing requests due to previous errors; if there are no queued
      // API operations pertaining to the users in this request, attempt to use
      // the API.
      if (!exact_target_soap_request_safe_for_live($uids)) {
        // Write the request to the batch table so it can be processed later.
        $row = new stdClass();
        $row->method = $method;
        $row->params = serialize($params);
        $row->queued = $_SERVER['REQUEST_TIME'];

        // If there's more than one user affected by this request set the uid to
        // NULL - this will require a full batch clear before the site returns
        // to "live mode".
        $row->uid = (count($uids) == 1) ? $uids[0] : NULL;
        drupal_write_record('exact_target_soap_batch', $row);

        $result = new stdClass();
        $result->OverallStatus = 'queued';
        return $result;
      }
    }

    // While we're still within a time threshold we'll continue retrying the request
    // if it fails. A successful request or one which exceeds the threshold will
    // return, breaking the loop.
    $start = microtime(TRUE);
    $verbose = variable_get('exact_target_soap_verbose', FALSE);
    $allow_retry = variable_get('exact_target_soap_allow_retry', TRUE);
    while (TRUE) {
      try {
        $method = ucwords($method);
        $ret = call_user_func_array(array($this->client, $method), $params);
        if ($verbose) {
          watchdog(
            'exact_target_soap',
            'Successful @type request took @time seconds.',
            array('@type' => $method, '@time' => (microtime(TRUE) - $start))
          );
        }
        return $ret;
      }
      catch (SoapFault $e) {
        // Retry a failed request if we're still within a threshold and the failure was
        // a transport error.
        if ($allow_retry && (microtime(TRUE) - $start) < variable_get('exact_target_soap_retry', 2.5) &&
          ($e->faultcode === 'HTTP' || !exact_target_soap_status())
        ) {
          if ($verbose) {
            watchdog(
              'exact_target_soap',
              'Retrying @type request, druation: @time seconds.',
              array('@type' => $method, '@time' => (microtime(TRUE) - $start)),
              WATCHDOG_WARNING
            );
          }
          continue;
        }

        if ($verbose) {
          watchdog(
            'exact_target_soap',
            'Failed @type request took @time seconds.',
            array('@type' => $method, '@time' => (microtime(TRUE) - $start)),
            WATCHDOG_ERROR
          );
        }
        // Put the site in batch mode if there is a connection problem.
        if ($e->faultcode === 'HTTP') {
          variable_set('exact_target_soap_batch', TRUE);
          watchdog('exact_target_soap', 'Your site failed to connect to the ExactTarget API.', array(), WATCHDOG_ERROR);
        }

        // If the API is down, put the module into batch mode and queue the last
        // request. If the API was down for the previous call, but came back up
        // when we checked the status, we still want to batch the failed call.
        if (!$bypass_batch && $method != 'GetSystemStatus' && ($e->faultcode === 'HTTP' || !exact_target_soap_status())) {
          $row = new stdClass();
          $row->method = $method;
          $row->params = serialize($params);
          $row->queued = $_SERVER['REQUEST_TIME'];
          $row->uid = (count($uids) == 1) ? $uids[0] : NULL;
          drupal_write_record('exact_target_soap_batch', $row);
          $result = new stdClass();
          $result->OverallStatus = 'queued';
          return $result;
        }

        return FALSE;
      }
    }
  }

  public static function last($pretty = FALSE) {
    $last = (object) array(
      'headers' => self::instance()->client->__getLastRequestHeaders(),
      'request' => self::instance()->client->__getLastRequest(),
      'response' => self::instance()->client->__getLastResponse(),
    );

    if ($pretty) {
      return '<pre>' . highlight_string('<?php ' . var_export($last, TRUE)) . '</pre>';
    }

    return $last;
  }

  // @todo Simplify SoapVar encoding by doing it automagically
  // on every SOAP call.
  private function prepare_array(&$objects) {
    for ($x = 0; $x < count($objects); $x++) {
      $this->prepare($objects[$x]);
    }
  }

  private function prepare(&$object) {
    if (!is_object($object)) {
      return;
    }

    foreach (get_object_vars($object) as $prop => $value) {
      if (is_array($object->$prop)) {
        $this->prepare_array($object->$prop);
      }
      elseif (is_object($object->$prop)) {
        $this->prepare($object->$prop);
      }
    }

    $class = get_class($object);
    if (strpos($class, 'ExactTarget_') === 0) {
      $type = str_replace('ExactTarget_', '', $class);
      $object = self::encode($object, $type);
    }
  }

  public static function encode($object, $type = NULL) {
    if (is_null($type)) {
      $type = get_class($object);
      if (strpos($type, 'ExactTarget_') === 0) {
        $type = str_replace('ExactTarget_', '', $type);
      }
    }
    return new SoapVar($object, SOAP_ENC_OBJECT, $type, "http://exacttarget.com/wsdl/partnerAPI");
  }

}
