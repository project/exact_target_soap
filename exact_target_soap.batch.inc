<?php

/**
 * @file
 * Batch callbacks for the exact_target_soap module.
 */

/**
 * Processes the exact target batch queue.
 */
function exact_target_soap_batch_process($timeout = 0, &$context) {
  // If a process is currently running, bail.
  if (!lock_acquire('exact_target_soap_batch_semaphore', ($timeout ? $timeout : 600))) {
    watchdog(
      'exact_target_soap',
      'Attempt to clear the ExactTarget batch queue while another process is already running.',
      array(),
      WATCHDOG_ERROR
    );
    $context['success'] = FALSE;
    $context['finished'] = 1;
    return;
  }

  if (empty($context['sandbox'])) {
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['current_req'] = 0;
    $max = db_query("SELECT MAX(bid) AS bid, COUNT(bid) AS count FROM {exact_target_soap_batch}")->fetchObject();
    $context['sandbox']['count'] = $max->count;
    $context['sandbox']['max_bid'] = $max->bid;
    $context['sandbox']['status'] = exact_target_soap_status();
    $context['sandbox']['start_time'] = $_SERVER['REQUEST_TIME'];
    $context['sandbox']['drush'] = function_exists('drush_print');

    if (EXACT_TARGET_SOAP_ASYNCHRONOUS == variable_get('exact_target_soap_synchronous_api', EXACT_TARGET_SOAP_ASYNCHRONOUS)) {
      $context['sandbox']['request_type'] = ExactTarget_RequestType::Asynchronous;
    }
    else {
      $context['sandbox']['request_type'] = ExactTarget_RequestType::Synchronous;
    }
  }

  // If the API is down, break the process.
  if (!$context['sandbox']['status']) {
    $context['success'] = FALSE;
    $context['finished'] = 1;

    lock_release('exact_target_soap_batch_semaphore');
    return;
  }

  $limit = 20;
  $res = db_query_range("SELECT * FROM {exact_target_soap_batch} WHERE bid > :bid AND bid <= :mbid ORDER BY bid ASC", 0, $limit, array(':bid' => $context['sandbox']['current_req'], ':mbid' => $context['sandbox']['max_bid']));
  foreach ($res as $req) {
    $params = unserialize($req->params);
    $params[0]->exact_target_soap_bypass_batch = TRUE;

    // Synchronous API requests typically take a LONG time and are prone to
    // timeouts. Therefore, unless an administrator has chosen to run the site
    // in synchronous mode make absolutely sure that batched requests are
    // sent in asynchronous mode.
    if (isset($params[0]->Options)) {
      // Handling an array is a temporary solution. The code has been refactored
      // so that all API requests going forward should use some variation of a
      // request object. This code should be removed after the batch is cleared
      // in live.
      if (is_array($params[0]->Options)) {
        $options = new ExactTarget_Options();
        $options->SaveOptions = $params[0]->Options;
        $params[0]->Options = $options;
      }

      // Note: don't remove this part.
      $params[0]->Options->RequestType = $context['sandbox']['request_type'];
    }
    else {
      switch (get_class($params[0])) {
        case 'ExactTarget_CreateRequest':
          $options = new ExactTarget_CreateOptions();
          $options->RequestType = $context['sandbox']['request_type'];
          break;
        case 'ExactTarget_UpdateRequest':
          $options = new ExactTarget_UpdateOptions();
          $options->RequestType = $context['sandbox']['request_type'];
          break;
        case 'ExactTarget_DeleteRequest':
          $options = new ExactTarget_DeleteOptions();
          $options->RequestType = $context['sandbox']['request_type'];
          break;
        case 'ExactTarget_RetrieveRequest':
          $options = new ExactTarget_RetrieveOptions();
          $options->RequestType = $context['sandbox']['request_type'];
          break;
        case 'ExactTarget_ExtractRequest':
          $options = new ExactTarget_ExtractOptions();
          $options->RequestType = $context['sandbox']['request_type'];
          break;
        default:
          $options = new ExactTarget_Options();
          $options->RequestType = $context['sandbox']['request_type'];
          break;
      }
      $params[0]->Options = $options;
    }
    $result = ExactTarget::instance()->{$req->method}($params[0]);

    if ($result === FALSE) {
      watchdog('exact_target_soap', "ExactTarget API Error:\n@e", array('@e' => $e), WATCHDOG_ERROR);

      if (!exact_target_soap_status()) {
        $context['sandbox']['status'] = FALSE;

        lock_release('exact_target_soap_batch_semaphore');
        return;
      }
    }

    // Remove the request from the queue.
    db_delete('exact_target_soap_batch')
            ->condition('bid', $req->bid)
            ->execute();
    $context['sandbox']['progress']++;
    $context['sandbox']['current_req'] = $req->bid;

    // Limit the run time for this process. NOTE: time()
    // is purposely used because $_SERVER['REQUEST_TIME']
    // doesn't advance in drush/non-interactive mode.
    $time = REQUEST_TIME - $context['sandbox']['start_time'];
    if ($timeout > 0 && $time > $timeout) {
      if ($context['sandbox']['drush']) {
        $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['count'];
        drush_print(dt(
          '@count API requests processed, @percent% complete.',
          array('@count' => $context['sandbox']['progress'], '@percent' => number_format($context['finished'] * 100, 2))
        ));
      }
      watchdog(
        'exact_target_soap',
        'The batch process has been aborted because it has been running for @time seconds, exceeding the @timeout second limit.',
        array('@time' => $time, '@timeout' => $timeout),
        WATCHDOG_WARNING
      );
      $context['success'] = TRUE;
      $context['finished'] = 1;

      lock_release('exact_target_soap_batch_semaphore');
      return;
    }
  }
  if ($context['sandbox']['progress'] != $context['sandbox']['count']) {
    $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['count'];
    if ($context['sandbox']['drush']) {
      drush_print(dt(
        '@count API requests processed, @percent% complete.',
        array('@count' => $context['sandbox']['progress'], '@percent' => number_format($context['finished'] * 100, 2))
      ));
    }
  }

  lock_release('exact_target_soap_batch_semaphore');
}

/**
 * Returns the status and disables batch mode if the API is stable.
 */
function exact_target_soap_batch_finished($success, $results, $operations) {
  // This is likely redundant, but we want to make sure we release the lock
  // in the event of an unexpected failure.
  lock_release('exact_target_soap_batch_semaphore');

  $count = db_query("SELECT COUNT(bid) FROM {exact_target_soap_batch}")->fetchField();
  $t = function_exists('dt') ? 'dt' : 't';
  if ($success && !$count) {
    $message = $t('All queued requests have been processed.');
    $type = 'status';

    // Disable batch processing.
    $result = NULL;
    exact_target_soap_status($result, TRUE);
  }
  elseif ($success && $count) {
    $message = $t('The batch was processed but items remain in the queue. Please review for bad data.');
    $type = 'warning';
  }
  else {
    $message = $t('There was a problem processing the queue. The API may have gone down.');
    $type = 'error';
  }

  // Send a message if there are still items in the batch and the site admin
  // wants to be notified of this.
  if ($type != 'status' && variable_get('exact_target_soap_cleanup', EXACT_TARGET_SOAP_CLEANUP_NOTHING) != EXACT_TARGET_SOAP_CLEANUP_MAIL) {
    $to = variable_get('exact_target_soap_mail', variable_get('site_mail', ''));
    drupal_mail('exact_target_soap', 'failure', $to, language_default());
  }

  // Try to direct the output correctly.
  if (function_exists('drush_log')) {
    // Switch to Drush status type.
    if ($type == 'status') {
      $type = 'success';
    }
    drush_log($message, $type);
    if ($type == 'error') {
      drush_set_error('Batch processing error');
    }
  }
  else {
    drupal_set_message($message, $type);
  }
}

