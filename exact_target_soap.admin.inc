<?php

/**
 * @file
 * Admin callbacks for the exact_target_soap module.
 */

/**
 * Returns the system settings form.
 */
function exact_target_soap_settings() {
  $form = array();

  // Prevent displaying this warning more than once. The warning message is
  // incorrectly displayed if a batch was just processed -- probably the
  // message gets stored in the queue and not unset before the page is refreshed,
  // or the static variable cache doesn't update before the page is loaded.
  $messages = drupal_get_messages('warning', TRUE);
  if (isset($messages['warning']) && is_array($messages['warning']) && sizeof($messages['warning'])) {
    foreach ($messages['warning'] as $warning) {
      if (strpos($warning, 'Your site is queueing ExactTarget requests.') === 0) {
        continue;
      }
      drupal_set_message(filter_xss($warning), 'warning');
    }
  }

  // Provide information about the state of the batch queue.
  $batch = variable_get('exact_target_soap_batch', FALSE);
  if (!empty($batch)) {
    $count = db_query("SELECT COUNT(*) FROM {exact_target_soap_batch}")->fetchField();
    $msg = 'Your site is queueing ExactTarget requests. The batch queue currently contains !count.';
    $args = array(
      '!count' => format_plural($count, 'one API request', '@count API requests'),
    );
    if ($count > 0) {
      $msg .= '  You should clear the batch as soon as possible.';
    }
    drupal_set_message(t($msg, $args), 'warning');
  }

  $form['batch'] = array(
    '#type' => 'fieldset',
    '#title' => t('Batch processing'),
    '#description' => t('Processes all queued ExactTarget API requests. Note: It is generally better practice to clear the ExactTarget queue using the included drush command: exact-target-soap-clear-batch (etc).'),
  );
  $form['batch']['clear'] = array(
    '#type' => 'submit',
    '#value' => t('Clear'),
    '#submit' => array('exact_target_soap_admin_clear_batch'),
  );
  
  $form['queue_status'] = array(
    '#type' => 'fieldset',
    '#title' => t('Queue status'),
    '#description' => t('Permanently remove all queued ExactTarget API requests without processing them.'),
  );
  $form['queue_status']['clear'] = array(
    '#type' => 'submit',
    '#value' => t('Remove all'),
    '#submit' => array('exact_target_soap_admin_clear_queue'),
  );

  $form['api'] = array(
    '#type' => 'fieldset',
    '#title' => t('API settings'),
  );
  $form['api']['exact_target_soap_wsdl'] = array(
    '#type' => 'textfield',
    '#title' => t('ExactTarget API WSDL'),
    '#default_value' => variable_get('exact_target_soap_wsdl', ''),
    '#description' => t('Path to the ExactTarget API WSDL endpoint.'),
    '#required' => TRUE,
  );
  $form['api']['exact_target_soap_username'] = array(
    '#type' => 'textfield',
    '#title' => t('ExactTarget API user'),
    '#default_value' => variable_get('exact_target_soap_username', ''),
    '#description' => t('The user name that is used to access the ExactTarget SOAP API.'),
  );
  $form['api']['exact_target_soap_password'] = array(
    '#type' => 'password',
    '#title' => t('ExactTarget API password'),
    '#default_value' => variable_get('exact_target_soap_password', ''),
    '#description' => t('The password that is used to access the ExactTarget SOAP API.'),
  );
  $form['api']['exact_target_soap_proxy_host'] = array(
    '#type' => 'textfield',
    '#title' => t('ExactTarget API proxy host'),
    '#default_value' => variable_get('exact_target_soap_proxy_host', ''),
    '#description' => t('The proxy host used to access the ExactTarget SOAP API.'),
  );
  $form['api']['exact_target_soap_proxy_port'] = array(
    '#type' => 'textfield',
    '#title' => t('ExactTarget API proxy port'),
    '#default_value' => variable_get('exact_target_soap_proxy_port', ''),
    '#description' => t('The proxy port used to access the ExactTarget SOAP API.'),
  );
  $form['api']['exact_target_soap_proxy_login'] = array(
    '#type' => 'textfield',
    '#title' => t('ExactTarget API proxy login'),
    '#default_value' => variable_get('exact_target_soap_proxy_login', ''),
    '#description' => t('The proxy login used to access the ExactTarget SOAP API.'),
  );
  $form['api']['exact_target_soap_proxy_password'] = array(
    '#type' => 'password',
    '#title' => t('ExactTarget API proxy password'),
    '#default_value' => variable_get('exact_target_soap_proxy_password', ''),
    '#description' => t('The proxy password used to access the ExactTarget SOAP API.'),
  );
  $form['api']['exact_target_soap_synchronous_api'] = array(
    '#type' => 'radios',
    '#title' => t('ExactTarget API Synchronous Communication'),
    '#options' => array(
      EXACT_TARGET_SOAP_ASYNCHRONOUS => t('Use Asynchronous Communication (normal setting)'),
      EXACT_TARGET_SOAP_SYNCHRONOUS => t('Use Synchronous Communication (better debugging)'),
    ),
    '#default_value' => variable_get('exact_target_soap_synchronous_api', EXACT_TARGET_SOAP_ASYNCHRONOUS),
    '#description' => t('Sets the communication with Exact Target into synchronous or asynchronous mode.  Asyncronous mode is more reliable and preferred, synchronous mode produces better debugging messages if there are problems.'),
  );
  $form['api']['exact_target_soap_verbose'] = array(
    '#type' => 'checkbox',
    '#title' => t('Verbose logging'),
    '#default_value' => variable_get('exact_target_soap_verbose', FALSE),
    '#description' => t('Enables logging of types of requests and the time to fly for successful, retry, and failed requests.'),
  );
  $form['api']['exact_target_soap_allow_retry'] = array(
    '#type' => 'checkbox',
    '#title' => t('Retry failed requests'),
    '#default_value' => variable_get('exact_target_soap_allow_retry', TRUE),
    '#description' => t('Retries failed requests within a given time threshold.'),
  );
  if (variable_get('exact_target_soap_allow_retry', TRUE)) {
    $form['api']['exact_target_soap_retry'] = array(
      '#type' => 'textfield',
      '#title' => t('Retry timeout threshold'),
      '#default_value' => variable_get('exact_target_soap_retry', 2.5),
      '#description' => t('Length of time in seconds to allow failed API requets to retry.'),
    );
  }

  $form['cleanup'] = array(
    '#type' => 'fieldset',
    '#title' => t('Batch cleanup'),
  );
  $form['cleanup']['exact_target_soap_cleanup'] = array(
    '#type' => 'radios',
    '#title' => t('Batch cleanup options'),
    '#options' => array(
      EXACT_TARGET_SOAP_CLEANUP_NOTHING => t('Do nothing'),
      EXACT_TARGET_SOAP_CLEANUP_MAIL => t('Send email notification'),
      EXACT_TARGET_SOAP_CLEANUP_CRON => t('Clean on cron'),
    ),
    '#default_value' => variable_get('exact_target_soap_cleanup', EXACT_TARGET_SOAP_CLEANUP_NOTHING),
    '#description' => t('Action to take if the system goes into batch mode.'),
  );
  $form['cleanup']['exact_target_soap_mail'] = array(
    '#type' => 'textfield',
    '#title' => t('Notification email(s)'),
    '#default_value' => variable_get('exact_target_soap_mail', variable_get('site_mail', '')),
    '#description' => t('Enter the email addresses (comma separated) that you would like to notify if the system goes into batch mode.'),
  );

  return system_settings_form($form);
}

/**
 * Submit callback for Clear button on settings page.
 */
function exact_target_soap_admin_clear_batch($form, &$form_state) {
  exact_target_soap_clear_batch(TRUE, 30);
}

/**
 * Submit callback for Remove button on settings page.
 */
function exact_target_soap_admin_clear_queue($form, &$form_state) {
  $count = db_delete('exact_target_soap_batch')
            ->condition('bid', 0, '>')
            ->execute();
  if (!empty($count)) {
    drupal_set_message(t('@count queue items removed.', array('@count' => $count)));
    $form_state['redirect'] = 'admin/config/services/exact_target_soap';
  }
  else {
    drupal_set_message(t('There was a problem removing items from the queue.'), 'error');
  }
}

/**
 * Returns ExactTarget SOAP API connection status.
 */
function exact_target_soap_status_page() {
  // Take out of batch mode
  variable_set('exact_target_soap_batch', FALSE);
  $output = '';
  // exact_target_soap_status() only returns TRUE or FALSE without much detail
  $request = new ExactTarget_SystemStatusResponseMsg();

  try {
    $result = @ExactTarget::instance()->GetSystemStatus($request);
  } catch (Exception $e) {
    drupal_set_message(t('Your site failed to connect to the ExactTarget SOAP API.'), 'warning');
    return '';
  }

  if (!is_object($result) || $result->OverallStatus != 'OK') {
    drupal_set_message(t('There was a problem connecting to the ExactTarget SOAP API: @status: @message.'), array('@status' => filter_xss($result->OverallStatus), '@message' => filter_xss($result->OverallStatusMessage)), 'warning');
  }
  else {
    drupal_set_message(t('Your site is connected to the ExactTarget SOAP API.'));
  }

  $rr = new ExactTarget_RetrieveRequest();
  $rr->ObjectType = "DataExtension";
  // Set the properties to return
  // @see http://help.exacttarget.com/en/technical_library/web_service_guide/objects/dataextension/
  $props = array("ObjectID", "CustomerKey", "CategoryID", "Name", "Description", "IsSendable");
  $rr->Properties = $props;
  // Return all MIDs
  $rr->Filter = NULL;

  $rrm = new ExactTarget_RetrieveRequestMsg();
  $rrm->RetrieveRequest = $rr;
  try {
    $results = @ExactTarget::instance()->Retrieve($rrm);
  } catch (Exception $e) {
    $results = FALSE;
  }

  if (!empty($results)) {
    if ($results->OverallStatus == 'OK') {
      $rows = array();
      foreach ($results->Results as $de) {
        $row = array();
        $row['customerkey'] = $de->CustomerKey;
        $row['name'] = $de->Name;
        $row['category_id'] = isset($de->CategoryID) ? $de->CategoryID : '';
        $row['is_sendable'] = isset($de->IsSendable) ? $de->IsSendable : '';
        $row['description'] = isset($de->Description) ? $de->Description : '';
        $rows[] = $row;
      }
      $header = array(t('Customer Key'), t('Name'), t('CategoryID'), t('Sendable'), t('Description'));
      $output .= theme('table',  array('caption' => t('Available Data Extensions'), 'header' => $header, 'rows' => $rows, 'empty' => t('Unable to look up Field extension objects')));
    }
    else {
      drupal_set_message(filter_xss($results->OverallStatus), 'warning');
    }
  }

  return $output;
}

/**
 * Returns the list of queued SOAP requests.
 */
function exact_target_soap_queue() {
  $rows = array();
  $limit = 50;
  $header = array(
    array(
      'data' => t('ID'),
      'field' => 'bid',
      'sort' => 'asc',
    ),
    array(
      'data' => t('Operation'),
      'field' => 'method',
    ),
    t('Parameters'),
    array(
      'data' => t('Date queued'),
      'field' => 'queued',
    ),
    t('Options')
  );
  $query = db_select('exact_target_soap_batch', 'etsb')->extend('PagerDefault')->extend('TableSort');
  $results = $query->fields('etsb', array('bid', 'method', 'params', 'queued'))
          ->limit($limit)
          ->orderByHeader($header)
          ->execute();

  while ($record = $results->fetchAssoc()) {
    $row = array();
    $row['bid'] = $record['bid'];
    $row['method'] = $record['method'];
    $row['params'] = truncate_utf8($record['params'], 50, FALSE, TRUE);
    $row['queued'] = format_date($record['queued']);
    $links = array(
      'view' => array(
        'title' => 'view',
        'href' => 'admin/config/services/exact_target_soap/queue/' . $record['bid'],
      ),
      'delete' => array(
        'title' => 'delete',
        'href' => 'admin/config/services/exact_target_soap/queue/' . $record['bid'] . '/delete',
      ),
    );
    $row['options'] = theme('links', array('links' => $links, 'attributes' => array('class' => array('links', 'inline'))));
    $rows[] = $row;
  }

  return theme('table',  array('header' => $header, 'rows' => $rows, 'empty' => t('No queued SOAP requests'))) . theme('pager');
}

/**
 * Menu callback for admin/config/services/exact_target_soap/queue/%.
 * Returns information about a SOAP request in the batch queue.
 */
function exact_target_soap_queue_request_view($bid) {
  $request = db_query("SELECT * FROM {exact_target_soap_batch} WHERE bid = :bid", array(':bid' => $bid));
  $result = $request->fetchAssoc();
  if (empty($result)) {
    return drupal_not_found();
  }

  foreach ($result as $k => &$v) {
    if ($k == 'queued') {
      $v = format_date($v);
    }
    $v = $k . ': ' . $v;
  }

  return theme('item_list', array('items' => $result));
}

/**
 * Returns a form to delete a SOAP request from the batch queue.
 */
function exact_target_soap_queue_request_delete($form, &$form_state, $bid) {
  $request = db_query("SELECT * FROM {exact_target_soap_batch} WHERE bid=:bid", array(':bid' => $bid))->fetchAssoc();
  if (!$request) {
    return drupal_not_found();
  }

  $form['request'] = array(
    '#type' => 'value',
    '#value' => $request,
  );
  $form = confirm_form(
    $form,
    t('Would you like to delete this request?'),
    'admin/config/services/exact_target_soap/queue',
    t('This action cannot be undone.'),
    t('Delete')
  );

  return $form;
}

/**
 * Submit handler for exact_target_soap_queue_request.
 * @param array $form
 * @param array $form_state
 */
function exact_target_soap_queue_request_delete_submit($form, &$form_state) {
  $count = db_delete('exact_target_soap_batch')
            ->condition('bid', $form_state['values']['request']['bid'])
            ->execute();
  if (!empty($count)) {
    drupal_set_message(t('The SOAP request has been deleted.'));
    $form_state['redirect'] = 'admin/config/services/exact_target_soap/queue';
  }
  else {
    drupal_set_message(t('There was a problem deleting the SOAP request.'), 'error');
  }
}

